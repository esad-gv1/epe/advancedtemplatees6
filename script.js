import { loadScripts } from "./js/utils.js";
import { paginate } from "./js/paginate.js";

const elementsToPaginate = [];

//CSS used in this project, including the pagedjs preview css
const styleList = [
    "css/style.css",
    "css/cover.css",
    "css/back.css",
    "vendors/css/paged-preview.css"
];

//additional script to load, not importable as es modules
const scritpList = [
    "vendors/js/markdown-it.js",
    "vendors/js/markdown-it-footnote.js",
    "vendors/js/markdown-it-attrs.js",
    "vendors/js/markdown-it-container.js"
];

//sync batch loading
await loadScripts(scritpList);

//markdown files to load
const mdFilesList = ["md/cover.md", "md/main.md", "md/back.md"];
//html elements to be filled from converted md file
const partsList = ["cover", 'content', "back"];

//markdownit instanciation (old school method as no ES6 modules are available)
const markdownit = window.markdownit
    ({
        // Options for markdownit
        langPrefix: 'language-fr',
        // You can use html markup element
        html: true,
        typographer: true,
        // Replace english quotation by french quotation
        quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
    })
    .use(markdownitContainer) //div
    .use(markdownItAttrs, { //custom html element attributes
        // optional, these are default options
        leftDelimiter: '{',
        rightDelimiter: '}',
        allowedAttributes: [] // empty array = all attributes are allowed
    });

async function layoutHTML() {

    //!important! forEach can't be used as it doesn't respect await order!
    for (let index = 0; index < mdFilesList.length; index++) {

        const mdFile = mdFilesList[index];
        const reponse = await fetch(mdFile);
        const mdContent = await reponse.text();
        //convertion from md to html, returns a string
        const result = markdownit.render(mdContent);

        const destinationElement = document.getElementById(partsList[index]);
        destinationElement.innerHTML = result;
        elementsToPaginate.push(destinationElement.cloneNode(true));
    };

}

window.addEventListener("load", async (event) => {
    await layoutHTML();
});

window.addEventListener("click", () =>{
    paginate(elementsToPaginate, styleList);
});

