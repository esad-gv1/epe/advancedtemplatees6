# EPE templates

## Advanced Template written in ES6

This template aims to make an interactive website printable. Any kind of elements composing a web page can be given to Paged.js for pagination. Therefore, any kind of website could be printable, wether its content is static (usual HTML), generated from server side or through a local algorithm of any kind (using JavaScript).
Furthermore, as Paged.js can be extended thanks to a handler system, special processes to apply on the pagination execution can be programmed as well. To make it easier to build, a simple system of custom handlers has been defined in this template to help creative coders to make their own.

To give the EPE tools some coherence, the same structure used in [basictemplatees6](https://gitlab.com/esad-gv1/epe/basictemplatees6) is used, but it's only to give some sample usage and is meant to be changed accordingly to your own project.

[https://gitlab.com/esad-gv1/epe/advancedtemplatees6](https://gitlab.com/esad-gv1/epe/advancedtemplatees6)

## Usage

Download this folder (in zip format, for example).

`index.html` uses an `importmap` to use the `esm` version of Paged.js, that is far more flexible than the polyfill version we use in the basic templates.

```html
<script type="importmap">
{
    "imports" : {
        "pagedjs" : "./vendors/js/paged.esm.js"
    }
}
</script>
```
The main `script.js` is loaded as a `module` to make the whole chain ES6 compilant.

markdown files are in the `md` folder and loaded dynamically :

```javascript
//markdown files to load
const mdFilesList = ["md/cover.md", "md/main.md", "md/back.md"];
```

Their content is converted by `markdown-it` and used to fill the 3 elements inside prepared in the `body` by the `layoutHTML` function defined in the script.

The `layoutHTML` function is called automatically on page loading, **but you could call it at any other moment you want**. It is designed to synchronously :

* load the md files;
* convert md to html
* fill the html elements in the `body`
* clone all the elements and keep them in an array

The results is the rendering of the md files in the page and to keep a clone of them for a later process, i.e. pagination with Paged.js.

The `paginate.js` file contains all the Paged.js related code. The `paginate` function of it is exported to be called at any time, by any handler you want. It is designed to maintain a `DocumentFragment` element internally that can be filled by an arbitrary array of elements :

```javascript
export async function paginate(elementsList, styleList)
```

This `DocumentFragment` is passed to the Paged.js Previewer, along with user defined styles. Note that this will wipe out the body content to let the Paged.js Previewer fill the `document.body`.

## Handlers

`HandlerSkeleton.js` is a simple file that demonstrate how to use a handler to customize some part of the pagination process made by Paged.js. This file is meant to be duplicated and renamed to your convenience.
The EPE dev team is using this system to propose some custom extensions of Paged.js, like a highlighter for margins or a layer system.

To use a Handler, it must be imported in the `paginate.js` file then registered in the Paged.js handler system :

```javascript
import { HandlerSkeleton } from "./HandlerSkeleton.js";

const paged = new Previewer();
//register a handler to define hooks on a specific method it defines
registerHandlers(HandlerSkeleton);s
```

```
.
├── css
│   ├── back.css
│   ├── cover.css
│   ├── fonts
│   │   └── Inter-VariableFont_slnt_wght.ttf
│   └── style.css
├── index.html
├── js
│   ├── HandlerSkeleton.js
│   ├── paginate.js
│   └── utils.js
├── md
│   ├── back.md
│   ├── cover.md
│   └── main.md
├── readMe.md
├── script.js
└── vendors
    ├── css
    │   ├── paged-preview.css
    │   └── pagedjs-interface.css
    └── js
        ├── markdown-it-attrs.js
        ├── markdown-it-bracketed-spans.js
        ├── markdown-it-container.js
        ├── markdown-it-footnote.js
        ├── markdown-it-span.js
        ├── markdown-it.js
        ├── paged.esm.js
```

## Credits

* [Markdown-it](https://www.npmjs.com/package/markdown-it) for converting markdown to html in the browser;
* [Paged.js](https://pagedjs.org) for printed page layout and pdf generation;

## Authors

Dominique Cunin, Raphael Bottura
